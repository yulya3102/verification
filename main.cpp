#include "ltl.h"
#include "ltl_parser.hpp"
#include "kripke.h"
#include "buchi_ltl.h"
#include "buchi_kripke.h"
#include "verifier.h"

#include <buchi/buchi_loader.h>

#include <fstream>
#include <iostream>
#include <iterator>

int main(int argc, char ** argv)
{
    if (argc < 3)
    {
        std::cerr << "Usage: "
                  << argv[0] << " <kripke model> <ltl formulas> [<formula name>]"
                  << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream model_input(argv[1]);
    if (!model_input)
    {
        std::cerr << "Could not open file " << argv[1] << std::endl;
        return EXIT_FAILURE;
    }
    std::ifstream ltl_input(argv[2]);
    if (!ltl_input)
    {
        std::cerr << "Could not open file " << argv[2] << std::endl;
        return EXIT_FAILURE;
    }

    buchi::automaton<std::string> model = buchi::load_model(model_input);
    auto process_formula = [&] (auto const & formula_name, auto const & formula)
    {
        if(!ltl::containsTemporalOperators(formula)) {
            std::cout << formula_name << ": does not contain temporal operators, skipped" << std::endl;
            return;
        }
        auto result = find_counterexample(model, formula);
        if (!result)
            std::cout << formula_name << ": OK" << std::endl;
        else
        {
            std::cout << "Found counterexample for " << formula_name << ":" << std::endl;
            std::for_each(begin(*result), end(*result), [] (const auto & props)
            {
                if(props.size() == 0) std::cout << "(all props are false)";
                else std::copy(begin(props), end(props), std::ostream_iterator<std::string>(std::cout, ", "));
                std::cout << std::endl;
            });
        }
    };

    ltl::parser ltl_parser(ltl_input);
    if (argc == 4)
        process_formula(argv[3], ltl_parser.formulas[argv[3]]);
    else
        std::for_each(begin(ltl_parser.formulas), end(ltl_parser.formulas), [&] (const auto & formula)
        {
            process_formula(formula.first, formula.second);
        });

    return 0;
}
