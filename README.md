# LTL verifier

This is a LTL formula verifier and a couple of supporting tools.

Verification method: buchi automata is constructed from the input state machine and from the negation of LTL formula.
Then nonempty reachable cycles are found in the intersection of this two automatons, if there are any then there is a counterexample to the formula and it is false.

### Inputs
The models (automata) are encoded in XML, for examples see `./data/FSM` folder.

LTL formulas use simple text format, see examples in `./data/LTL` folder.
Following operators are supported:

| Syntax | Operation   |
|--------|-------------|
| not    | Negation    |
| X      | Next        |
| G      | Globally    |
| F      | Future      |
| R      | Release     |
| U      | Until       |
| or     | OR          | 
| and    | AND         |
| true   | TRUE        |
| false  | FALSE       |
| ->     | Implication |
| <->    | Equivalence |


## Applications

### vltl

Usage:

    vltl <kripke model> <ltl formulas> \[<formula name>]

Main application. Takes a single model as an input and a file with several LTL formulas, then each formula is verified using the method described above.
Name of a particular formula can be passed as a last argument, in this case only it will be checked.

If the application is built in debug mode, then the visualization of intermediate buchi automata it printed in the files in current directory, DOT file format. 


### buchi2dot

Usage: buchi2dot <input state machine> <output dot file>

Helper application that takes an XML file with state machine definition and outputs a DOT file with visualization.
  
Useful to generate pictures from automata.

## Example

Example model is included, see files `./data/FSM/Lock.xstd` and `./data/FSM/LockStarving.xstd`.
The corresponding formulas are in `./data/LTL/Lock.ltl`.

There is a visualization of the model in file `./data/Lock_illustration.jpg`.
Two workers are trying to enter a critical section and do some work.
Entering critical section is marked as `c1` and `c2` (for the first and second worker).
`w1` and `w2` mark waiting state for workers.

Note that the state (c1, c2) in unreachable -- we assume that tho workers can not enter a critical section simultaneously.
`safety` formula in LTL file checks that this holds globally.

Also there are a couple of formulas for starvation and for progress checking in this model.
