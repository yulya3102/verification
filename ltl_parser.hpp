#pragma once

#include <unordered_map>
#include <istream>
#include <sstream>
#include "ltl.h"


namespace ltl
{
enum TOKEN_TYPE
{
    END,
    SEMICOLON,
    STRING_LITERAL, TRUE, FALSE,
    VARNAME,
    ASSIGN,
    BRACKET_OPEN, BRACKET_CLOSE,
    NOT, X, G, F,
    R, U, OR, AND, IMP, EQ
};

struct ltl_token
{
    ltl_token(const TOKEN_TYPE & type) : type(type) {}
    ltl_token(const TOKEN_TYPE & type, const std::string & text) : type(type), text(text) {}

    TOKEN_TYPE type;
    std::string text;

    bool isUnary() const
    {
        return (type == NOT) || (type == X) || (type == G) || (type == F);
    }

    bool isBinary() const
    {
        return (type == R) || (type == U) || (type == OR) || (type == AND) || (type == IMP) || (type == EQ);
    }

    int precedence() const
    {
        switch(type)
        {
            case NOT:
            case X:
            case G:
            case F:
                return 4;
            case OR:
                return 0;
            case AND:
                return 1;
            case IMP:
            case EQ:
                return 2;
            case R:
            case U:
                return 3;
            default:
                return -1;
        }
    }

    // return 1 for left and 0 for right
    int associativity() const
    {
        switch(type)
        {
            case OR:
                return 1;
            case AND:
                return 1;
            case IMP:
            case EQ:
                return 1;
            case R:
            case U:
                return 1;
            default:
                return 1;
        }
    }
};

std::ostream& operator<< (std::ostream& stream, const ltl_token& token)
{
    stream << "{" << token.type << " " << token.text << "}";
    return stream;
}

struct ltl_lexer
{
    ltl_lexer(std::istream & input) : input(input), next_token(ltl_token(END))
    {
        consume();
    }

    ltl_token next() const
    {
        return next_token;
    }

    void consume()
    {
        auto str = read_word();
        TOKEN_TYPE type;

        if(str.empty()) type = END;
        else if(str.back() == '"')
        {
            str.pop_back();
            type = STRING_LITERAL;
        }
        else if(str == ";") type = SEMICOLON;
        else if(str == "=") type = ASSIGN;
        else if(str == "(") type = BRACKET_OPEN;
        else if(str == ")") type = BRACKET_CLOSE;
        else if(str == "not") type = NOT;
        else if(str == "X") type = X;
        else if(str == "G") type = G;
        else if(str == "F") type = F;
        else if(str == "R") type = R;
        else if(str == "U") type = U;
        else if(str == "or") type = OR;
        else if(str == "and") type = AND;
        else if(str == "true") type = TRUE;
        else if(str == "false") type = FALSE;
        else if(str == "->") type = IMP;
        else if(str == "<->") type = EQ;
        else type = VARNAME;
        next_token = ltl_token(type, str);
    }

    void die()
    {
        throw std::invalid_argument(next().text);
    }

private:
    std::string read_word()
    {
        consume_whitespace();
        std::stringbuf buffer;
        char c;
        while(input.get(c))
        {
            if(isspace(c)) break;
            if(ispunct(c))
            {
                if(c == '"')
                {
                    while(input.get(c) && c != '"') buffer.sputc(c);
                    buffer.sputc('"');
                    break;
                }
                else
                {
                    if(buffer.in_avail() == 0) buffer.sputc(c);
                    else input.putback(c);
                    break;
                }
            }
            buffer.sputc(c);
        }
        std::string res = buffer.str();
        if(res == "<" || res == "-") return res + read_word();
        return res;
    }

    void consume_whitespace()
    {
        char c;
        while(input.get(c))
        {
            if(isspace(c)) continue;
            input.putback(c);
            break;
        }
    }


    ltl_token next_token;
    std::istream &input;
};

// http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm#climbing
struct parser
{
    parser(std::istream & input) : lexer(ltl_lexer(input))
    {
        while(lexer.next().type != END) parse();
    }

    std::unordered_map<std::string, std::string> propositions;
    std::unordered_map<std::string, ltl::formula_ptr<std::string>> formulas;

private:
    ltl_lexer lexer;

    void parse()
    {
        auto name = expect(VARNAME).text;
        expect(ASSIGN);
        auto value = lexer.next();
        if(value.type == STRING_LITERAL)
        {
            propositions.insert(std::make_pair(name, value.text));
            consume();
        }
        else
        {
            auto formula = parse_formula();
            formulas.insert(std::make_pair(name, formula));
        }
        expect(SEMICOLON);
    }

    ltl::formula_ptr<std::string> parse_formula()
    {
        return parse_formula_e(0);
    }

    ltl::formula_ptr<std::string> parse_formula_e(int level)
    {
        auto t = parse_formula_p();
        while(true)
        {
            const auto next = lexer.next();
            if(!(next.isBinary() && next.precedence() >= level)) break;
            consume();
            int q = next.precedence() + next.associativity(); // +1 if left associative
            const auto &t1 = parse_formula_e(q);
            switch(next.type)
            {
                case OR:
                    t = t || t1;
                    break;
                case AND:
                    t = t && t1;
                    break;
                case IMP:
                    t = t >> t1;
                    break;
                case EQ:
                    t = ltl::eq(t, t1);
                    break;
                case R:
                    t = ltl::release(t, t1);
                    break;
                case U:
                    t = ltl::until(t, t1);
                    break;
                default: lexer.die(); // should never happen
            }
        }
        return t;
    }

    ltl::formula_ptr<std::string> parse_formula_p()
    {
        const auto next = lexer.next();
        if(next.isUnary())
        {
            consume();
            const auto &t = parse_formula_e(next.precedence());
            switch(next.type)
            {
                case NOT:
                    return ~t;
                case X:
                    return ltl::next(t);
                case G:
                    return ltl::globally(t);
                case F:
                    return ltl::future(t);
                default: lexer.die(); // should never happen
            }
        }
        else if(next.type == BRACKET_OPEN)
        {
            consume();
            const auto &t = parse_formula_e(0);
            expect(BRACKET_CLOSE);
            return t;
        }
        else if(next.type == VARNAME)
        {
            consume();
            return ltl::prop(next.text);
        }
        else if(next.type == TRUE)
        {
            consume();
            return ltl::boolean<std::string>(true);
        }
        else if(next.type == FALSE)
        {
            consume();
            return ltl::boolean<std::string>(false);
        }
        else lexer.die();
    }

    ltl_token expect(TOKEN_TYPE type)
    {
        auto token = lexer.next();
        if(token.type != type) lexer.die();
        lexer.consume();
        return token;
    }

    void consume()
    {
        lexer.consume();
    }
};
}
