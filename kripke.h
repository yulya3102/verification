#pragma once

#include <vector>
#include <unordered_map>

template <typename Prop>
struct kripke_model
{
    using Labels = std::vector<Prop>;
    using state_t = std::size_t;
    using states_t = std::vector<std::size_t>;

    kripke_model(std::size_t states_count,
          const states_t & initial_states,
          const std::unordered_map<state_t, states_t> & transition,
          const std::unordered_map<state_t, Labels> & labels)
        : states_count(states_count)
        , initial_states(initial_states)
        , transition(transition)
        , labels(labels)
    {}

    std::size_t states_count;
    states_t initial_states;
    std::unordered_map<state_t, states_t> transition;
    std::unordered_map<state_t, Labels> labels;
};
