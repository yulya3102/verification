#include <buchi/buchi_loader.h>

#include <iostream>
#include <fstream>
#include <stdexcept>

int main(int argc, char ** argv)
{
    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " <input state machine> <output dot file>" << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream input(argv[1]);
    if (!input)
        throw std::runtime_error("could not open input file");
    buchi::automaton<std::string> automaton
            = buchi::load_model(input);
    std::ofstream output(argv[2]);
    if (!output)
        throw std::runtime_error("could not open output file");
    output << automaton << std::endl;
    return 0;
}
