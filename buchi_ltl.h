#pragma once

#include "ltl.h"

#include <buchi/buchi.h>

#include <boost/functional/hash.hpp>

#include <vector>
#include <queue>
#include <algorithm>
#include <iterator>

namespace buchi
{
namespace detail
{
template <typename Prop>
struct generalized_automaton
{
    using state_t = std::size_t;
    using states_t = std::vector<state_t>;
    using Props = std::set<Prop>;
    using TrueProps = Props;
    using TransitionKey = std::pair<state_t, TrueProps>;

    generalized_automaton(const Props & all_props)
        : states_count(1)
        , initial_state(0)
        , all_props(all_props)
    {}

    std::size_t states_count;
    state_t initial_state;
    std::unordered_multimap<TransitionKey, state_t, boost::hash<TransitionKey>> transition;
    std::vector<states_t> final_states;
    Props all_props;
};

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_now_1(const ltl::until_formula<Prop> & f)
{
    return { f.f1 };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_next_1(const ltl::until_formula<Prop> & f)
{
    return { ltl::until(f.f1, f.f2) };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_now_2(const ltl::until_formula<Prop> & f)
{
    return { f.f2 };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_now_1(const ltl::release_formula<Prop> & f)
{
    return { f.f2 };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_next_1(const ltl::release_formula<Prop> & f)
{
    return { ltl::release(f.f1, f.f2) };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_now_2(const ltl::release_formula<Prop> & f)
{
    return { f.f1, f.f2 };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_now_1(const ltl::or_formula<Prop> & f)
{
    return { f.f1 };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_next_1(const ltl::or_formula<Prop> & f)
{
    return { };
}

template <typename Prop>
std::vector<ltl::formula_ptr<Prop>> true_now_2(const ltl::or_formula<Prop> & f)
{
    return { f.f2 };
}

template <typename T>
std::vector<std::set<T>> generate_subsets(std::set<T> set)
{
    if (set.empty())
        return {{}};

    T first = *set.begin();
    set.erase(set.begin());

    std::vector<std::set<T>> result = generate_subsets(set);
    std::size_t n = result.size();
    for (std::size_t i = 0; i < n; ++i)
    {
        auto with_first = result[i];
        with_first.insert(first);
        result.push_back(with_first);
    }

    return result;
}

template <typename Prop>
struct builder
{
    using Formula = ltl::formula_ptr<Prop>;
    using Formulas = std::set<Formula, ltl::formula_comparator<Prop>>;

    void add_state(std::queue<Formula> queue,
                   Formulas true_now,
                   Formulas true_next,
                   std::size_t pred)
    {
        while (!queue.empty())
        {
            Formula f = queue.front();
            queue.pop();
            true_now.insert(f);

            using namespace ltl;
#define cast(from, to, var) to var = dynamic_cast<to>(from.get())
            {
                cast(f, false_formula<Prop> *, ff);
                if (ff || true_now.find(~f) != true_now.end())
                    return; // inconsistent state
            }

            {
                cast(f, neg_proposition_formula<Prop> *, np);
                cast(f, proposition_formula<Prop> *, p);
                cast(f, true_formula<Prop> *, tf);
                if (np || p || tf)
                    continue;
            }

            {
                cast(f, or_formula<Prop> *, of);
                cast(f, until_formula<Prop> *, uf);
                cast(f, release_formula<Prop> *, rf);
                if (of || uf || rf)
                {
#define call(func) (of ? func(*of) : uf ? func(*uf) : func(*rf))
                    {
                        std::queue<Formula> new_queue(queue);
                        for (auto const & g : call(true_now_2))
                            new_queue.push(g);
                        add_state(new_queue, true_now, true_next, pred);
                    }

                    for (auto const & g : call(true_now_1))
                        queue.push(g);
                    for (auto const & g : call(true_next_1))
                        true_next.insert(g);
                    continue;
#undef call
                }
            }

            {
                cast(f, and_formula<Prop> *, af);
                if (af)
                {
                    queue.push(af->f1);
                    queue.push(af->f2);
                    continue;
                }
            }

            {
                cast(f, next_formula<Prop> *, nf);
                if (nf)
                {
                    true_next.insert(nf->f);
                    continue;
                }
            }
            throw std::runtime_error("missing cases in buchi::detail::builder::add_state");
#undef cast
        }

        auto state_desc = std::make_pair(true_now, true_next);
        auto q = states.find(state_desc);
        if (q != states.end())
        {
            q->second.second.push_back(pred);
            return;
        }
        auto state = states.size() + 1;
        states[state_desc].first = state;
        states[state_desc].second.push_back(pred);
        for (auto const & g : true_next)
            queue.push(g);
        add_state(queue, {}, {}, states[state_desc].first);
    }

    generalized_automaton<Prop> operator() (const std::set<Prop> & props, const Formula & f)
    {
        auto formula_props = ltl::all_props(f);
        if (!std::includes(props.begin(), props.end(),
                           formula_props.begin(), formula_props.end()))
            throw std::runtime_error(f->to_string() + " contains propositions that are not present in the model");

        generalized_automaton<Prop> result(props);

        std::queue<Formula> queue;
        queue.push(f);
        add_state(queue, {}, {}, result.initial_state);

        result.states_count += states.size();
        for (auto const & state : states)
        {
            auto to = state.second.first;
            auto const & true_now = state.first.first;
            auto const & incoming = state.second.second;

            using Props = typename generalized_automaton<Prop>::Props;

            Props max_t_props = result.all_props;
            auto f_props = ltl::false_props<Prop>(true_now.begin(), true_now.end());
            auto t_props = ltl::true_props<Prop>(true_now.begin(), true_now.end());
            for (auto const & prop : f_props)
                max_t_props.erase(prop);

            Props props_diff;
            std::set_difference(max_t_props.begin(), max_t_props.end(),
                                t_props.begin(), t_props.end(),
                                std::inserter(props_diff, props_diff.begin()));

            std::vector<Props> diff_subsets = generate_subsets(props_diff);

            for (auto const & diff_subset : diff_subsets)
            {
                auto props(t_props);
                props.insert(diff_subset.begin(), diff_subset.end());
                std::transform(incoming.begin(), incoming.end(), std::inserter(result.transition, result.transition.begin()),
                               [&] (const auto & from) { return std::make_pair(std::make_pair(from, props), to); });
            }
        }

        std::vector<ltl::formula_ptr<Prop>> subformulas = f->subformulas();
        subformulas.push_back(f);
        for (const auto & g : subformulas)
        {
            auto until = dynamic_cast<ltl::until_formula<Prop> *>(g.get());
            if (!until)
                continue;

            typename generalized_automaton<Prop>::states_t final_set;
            for (const auto & state : states)
            {
                auto const & true_now = state.first.first;
                if (true_now.find(g) != true_now.end() &&
                    true_now.find(until->f2) == true_now.end())
                    continue;
                final_set.push_back(state.second.first);
            }
            result.final_states.push_back(final_set);
        }

        return result;
    }

    using TrueNow = Formulas;
    using TrueNext = Formulas;
    using StateDesc = std::pair<TrueNow, TrueNext>;
    using State = std::pair<typename generalized_automaton<Prop>::state_t,
                            typename generalized_automaton<Prop>::states_t>;
    std::unordered_map<StateDesc, State, boost::hash<StateDesc>> states;
};

template <typename Prop>
automaton<Prop> from_generalized(const generalized_automaton<Prop> & ga)
{
    using State = typename automaton<Prop>::state_t;
    std::size_t n = ga.final_states.size();
    automaton<Prop> result(ga.all_props);
    if (n > 0)
    {
        auto transform_state = [&] (State q, std::size_t i) { return i * ga.states_count + q; };

        result.initial_state = transform_state(ga.initial_state, 0);
        result.states_count = ga.states_count * n;
        for (std::size_t i = 0; i < n; ++i)
        {
            auto const & final_set_i = ga.final_states[i];
            std::for_each(begin(ga.transition), end(ga.transition),
                           [&] (const auto & edge)
            {
                auto const & from = std::get<0>(edge.first);
                auto const & true_props = std::get<1>(edge.first);
                auto const & to = edge.second;

                std::size_t j = (std::find(begin(final_set_i), end(final_set_i), from) != final_set_i.end()) ?
                                ((i + 1) % n) : i;
                auto new_from = transform_state(from , i);
                auto new_to = transform_state(to , j);
                result.transition[new_from].insert(std::make_pair(true_props, new_to));
            });
        }
        std::transform(begin(ga.final_states[0]), end(ga.final_states[0]), std::back_inserter(result.final_states),
                       [&] (const auto & final_state)
        {
            return transform_state(final_state, 0);
        });
    }
    else
    {
        result.initial_state = 0;
        result.states_count = 1;
        {
            decltype(result.all_props) t_props;
            result.transition[0].insert(std::make_pair(t_props, 0));
            for (auto const & prop : result.all_props)
            {
                t_props.insert(prop);
                result.transition[0].insert(std::make_pair(t_props, 0));
            }
        }
        result.final_states.push_back(result.initial_state);
    }
    return result;
}
}

template <typename Prop>
automaton<Prop> from_ltl(const std::set<Prop> & props, const ltl::formula_ptr<Prop> & f)
{
    detail::builder<Prop> builder;
    return detail::from_generalized(builder(props, f));
}
}
