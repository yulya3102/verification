#pragma once

#include <boost/functional/hash.hpp>
#include <boost/optional.hpp>

#include <vector>
#include <unordered_map>
#include <set>
#include <tuple>
#include <algorithm>
#include <queue>
#include <iterator>
#include <sstream>

namespace buchi
{
template <typename Prop>
struct automaton
{
    using state_t = std::size_t;
    using states_t = std::vector<state_t>;
    using Props = std::set<Prop>;
    using TrueProps = Props;
    using TransitionLabel = TrueProps;
    using Word = std::vector<Props>;
    using OutgoingTransitions = std::unordered_multimap<TransitionLabel, state_t, boost::hash<TransitionLabel>>;
    using Transitions = std::unordered_map<state_t, OutgoingTransitions>;

    automaton(const Props & all_props)
        : states_count(1)
        , initial_state(0)
        , all_props(all_props)
    {}

    std::size_t states_count;
    state_t initial_state;
    Transitions transition;
    states_t final_states;
    Props all_props;
#ifdef DEBUG
    std::unordered_map<state_t, std::string> state_names;
#endif
};


template <typename Prop>
std::basic_ostream<char> & operator<< (std::basic_ostream<char> & out, const buchi::automaton<Prop> & a)
{
    out << "digraph G {" << std::endl;
    out << "node [ label = \"\" ]" << std::endl;
    out << "init [ style = invis ]" << std::endl;
    out << "init -> s" << a.initial_state << std::endl;
    std::for_each(std::begin(a.transition), std::end(a.transition),
                  [&] (const auto & transition_from)
    {
        auto const & from = transition_from.first;
        auto const & transitions = transition_from.second;
        std::transform(begin(transitions), end(transitions),
                       std::ostream_iterator<std::string>(out, "\n"),
                       [&] (const auto & transition)
        {
            std::stringstream ss;
            auto & key = transition.first;
            auto & to = transition.second;
            auto & true_props = key;
            ss << "s" << from << " -> " << "s" << to << " [ label = \"";
            std::copy(true_props.begin(), true_props.end(),
                      std::ostream_iterator<Prop>(ss, ", "));
            ss << "\" ]";
            return ss.str();
        });
    });
    std::transform(std::begin(a.final_states), std::end(a.final_states),
                   std::ostream_iterator<std::string>(out, "\n"),
                   [&a] (std::size_t f) {
        std::stringstream ss;
        ss << "s" << f << " [ shape = doublecircle";
#ifdef DEBUG
        if(a.state_names.count(f)) ss << ", label=\"" << a.state_names.find(f)->second << "\"";
        else ss << ", label=\"" << std::to_string(f) << "\"";
#endif
        ss << " ]";
        return ss.str();
    });
#ifdef DEBUG
    for(size_t f = 0; f < a.states_count; f++)
    {
        if(std::find(a.final_states.begin(), a.final_states.end(), f) != a.final_states.end()) continue;
        if(a.state_names.count(f)) out << "s" << f << " [ label=\"" << a.state_names.find(f)->second << "\"]" << std::endl;
        else out << "s" << f << " [ label=\"" << std::to_string(f) << "\"]" << std::endl;
    }
#endif
    out << "}" << std::endl;

    return out;
}
}
