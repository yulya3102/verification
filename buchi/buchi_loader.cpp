#include "buchi_loader.h"
#include "tinyxml2.h"

#include <fsm/fsm.h>
#include <fsm/parser.h>

#include <istream>
#include <unordered_set>

namespace buchi
{
automaton<std::string> from_fsm(const fsm::automaton_t & fsm)
{
    automaton<std::string> result(fsm.all_labels);
    result.states_count = fsm.states.size() + 1;
    result.initial_state = fsm.states.size();
    std::for_each(begin(fsm.transition), end(fsm.transition), [&] (const auto & transition_from)
    {
        auto from = transition_from.first;
        const auto & outgoing = transition_from.second;
        std::for_each(begin(outgoing), end(outgoing), [&] (const auto & transition)
        {
            auto to = transition.second;
            auto true_props = transition.first;
            auto & new_prop = fsm.states[to].name;
            true_props.insert(true_props.begin(), new_prop);
            result.all_props.insert(result.all_props.begin(), new_prop);
            result.transition[from].insert(std::make_pair(true_props, to));
        });
    });
    auto & init_transitions = result.transition[result.initial_state];
    for (std::size_t i = 0; i < fsm.states.size(); ++i)
    {
        if (fsm.states[i].type == 1)
            init_transitions.insert({{}, i});
        //else if (fsm.states[i].type == 2)
        result.final_states.push_back(i);
    }

    return result;
}

automaton<std::string> load_model(std::istream & input)
{
    return from_fsm(fsm::load_model(input));
}

}
