#include "buchi.h"

#include <fsm/fsm.h>

#include <iostream>

namespace buchi
{
automaton<std::string> from_fsm(const fsm::automaton_t & fsm);
automaton<std::string> load_model(std::istream & input);
}
