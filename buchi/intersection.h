#include "buchi.h"

namespace buchi
{
namespace detail
{
template<typename Prop>
struct index_calculator
{
    index_calculator(size_t a, size_t b) : size1(a), size2(b) {}

    const size_t size1;
    const size_t size2;

    size_t total_size() const
    {
        return size1 * size2 * 3;
    }

    typename automaton<Prop>::state_t index(typename automaton<Prop>::state_t index1, typename automaton<Prop>::state_t index2, size_t index3) const
    {
        assert(index3 <= 2);
        return static_cast<typename automaton<Prop>::state_t>(index3 * (size1 * size2) + index2 * size1 + index1);
    }
};

template<typename T>
bool item_in_vector(const T &item, const std::vector<T> &vector)
{
    return std::find(vector.begin(), vector.end(), item) != vector.end();
}
}

template <typename Prop>
automaton<Prop> intersect(const automaton<Prop> & a, const automaton<Prop> & b)
{
    detail::index_calculator<Prop> ic(a.states_count, b.states_count);
    using res_t = automaton<Prop>;
    res_t result(a.all_props);
    std::copy(b.all_props.begin(), b.all_props.end(), std::inserter(result.all_props, result.all_props.begin()));

    result.states_count = ic.total_size();
    result.initial_state = ic.index(a.initial_state, b.initial_state, 0);

    const typename res_t::states_t &final_states_a = a.final_states;
    const typename res_t::states_t &final_states_b = b.final_states;


    for(typename res_t::state_t state_a = 0; state_a < a.states_count; state_a++)
    {
        for(typename res_t::state_t state_b = 0; state_b < b.states_count; state_b++)
        {
#ifdef DEBUG
            result.state_names.insert(std::make_pair(ic.index(state_a, state_b, 0), "(" + std::to_string(state_a) + "," + std::to_string(state_b) + ",0)"));
            result.state_names.insert(std::make_pair(ic.index(state_a, state_b, 1), "(" + std::to_string(state_a) + "," + std::to_string(state_b) + ",1)"));
            result.state_names.insert(std::make_pair(ic.index(state_a, state_b, 2), "(" + std::to_string(state_a) + "," + std::to_string(state_b) + ",2)"));
#endif
            result.final_states.push_back(ic.index(state_a, state_b, 2));
            const auto &transitions_from_state_a = a.transition.find(state_a)->second;
            const auto &transitions_from_state_b = b.transition.find(state_b)->second;
            for(const auto &transition_a: transitions_from_state_a)
            {
                const auto & key = transition_a.first;
                const auto & to = transition_a.second;
                const auto & true_props = key;
                for(const auto &transition_b : transitions_from_state_b)
                {
                    const auto &somewhere = transition_b.second;
                    if(key == transition_b.first)
                    {
                        auto add_transition = [&](typename res_t::state_t source, typename res_t::state_t target)
                        {
                            auto iter = result.transition.find(source);
                            if(iter == result.transition.end()) result.transition.insert(std::make_pair(source, typename res_t::OutgoingTransitions()));
                            auto &map = result.transition[source];
                            map.insert(std::make_pair(key, target));
                        };

                        size_t is_to_final = detail::item_in_vector(to, a.final_states) ? 1 : 0;
                        size_t is_somewhere_final = detail::item_in_vector(somewhere, b.final_states) ? 2 : 1;
                        add_transition(ic.index(state_a, state_b, 0), ic.index(to, somewhere, is_to_final));
                        add_transition(ic.index(state_a, state_b, 1), ic.index(to, somewhere, is_somewhere_final));
                        add_transition(ic.index(state_a, state_b, 2), ic.index(to, somewhere, 0));
                    }
                }
            }
        }
    }

    return result;
}

template <typename Prop>
automaton<Prop> operator && (const automaton<Prop> & a, const automaton<Prop> & b)
{
    return intersect(a, b);
}

}
