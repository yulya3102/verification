#include "buchi.h"

namespace buchi
{
namespace detail
{
template <typename Prop, typename OIter>
OIter find_nonempty_path(const automaton<Prop> & a,
                         typename automaton<Prop>::state_t from,
                         typename automaton<Prop>::state_t to,
                         OIter out)
{
    using buchi = automaton<Prop>;
    const auto undefined = a.states_count; // there is no node with such index
    std::vector<typename buchi::state_t> prev(a.states_count, undefined);
    std::vector<typename buchi::TrueProps> props_on_edge(a.states_count, typename buchi::TrueProps());
    std::queue<typename buchi::state_t> queue;
    queue.push(from);

    while(!queue.empty())
    {
        auto v = queue.front(); queue.pop();
        if(v == to && prev[v] != undefined)
        {
            while(prev[v] != undefined)
            {
                out = props_on_edge[v];
                const auto t = prev[v];
                prev[v] = undefined; // if from == to and this is a loop
                v = t;
            }
            break;
        }
        const auto &transitions = a.transition.find(v);
        if(transitions == a.transition.end()) continue;
        for(const auto &transition : transitions->second)
        {
            const auto &true_set = transition.first;
            const auto &target = transition.second;
            if(prev[target] != undefined) continue; // already in queue

            prev[target] = v;
            props_on_edge[target] = true_set;
            queue.push(target);
        }
    }
    return out;
}

template<typename Prop>
struct tarjan
{
    using graph = automaton<Prop>;

    tarjan(const graph &a) : a(a), v_index(a.states_count, undefined), v_lowlink(a.states_count, undefined),
                             v_onstack(a.states_count, false)
    {
        for (typename graph::state_t i = 0; i < a.states_count; i++)
        {
            if (v_index[i] == undefined) strong_connect(i);
        }
    }

    void strong_connect(typename graph::state_t v)
    {
        v_index[v] = index;
        v_lowlink[v] = index;
        index++;
        S.push_back(v);
        v_onstack[v] = true;

        for (const auto &transition_from : a.transition)
        {
            if (transition_from.first != v) continue;
            for (const auto & transition : transition_from.second)
            {
            typename graph::state_t w = transition.second;

            if (v_index[w] == undefined)
            {
                strong_connect(w);
                v_lowlink[v] = std::min(v_lowlink[v], v_lowlink[w]);
            } else if (v_onstack[w])
            {
                v_lowlink[v] = std::min(v_lowlink[v], v_lowlink[w]);
            }
            }
        }

        if (v_lowlink[v] == v_index[v])
        {
            typename graph::states_t component;
            typename graph::state_t w;
            do
            {
                w = S.back();
                S.pop_back();
                v_onstack[w] = false;
                component.push_back(w);
            } while (w != v);
            components.push_back(component);
        }
    }

    std::vector<typename graph::states_t> components;
    const graph &a;
    size_t index = 1;
    const size_t undefined = 0;
    std::vector<typename graph::state_t> S;
    std::vector<size_t> v_index;
    std::vector<size_t> v_lowlink;
    std::vector<bool> v_onstack;
};
}

template <typename Prop>
std::vector<typename automaton<Prop>::states_t>
    strongly_connected_components(const automaton<Prop> & a)
{
    return (detail::tarjan<Prop>(a)).components;
}

template <typename Prop>
boost::optional<typename automaton<Prop>::Word>
    find_example(const automaton<Prop> & a)
{
    auto sccs = strongly_connected_components(a);
    auto non_trivial = [&] (auto const & scc)
    {
        if (scc.size() > 1)
            return true;

        auto v = scc.front();
        if (a.transition.find(v) == a.transition.end())
            return false;

        const auto & v_transitions = a.transition.at(v);
        return std::any_of(v_transitions.begin(), v_transitions.end(),
                           [v] (const auto & transition)
                           { return transition.second == v; });
    };
    typename automaton<Prop>::Word result;
    auto final_state = std::find_first_of(begin(a.final_states), end(a.final_states),
                                          begin(sccs), end(sccs),
                                          [&] (auto const & f, auto const & scc)
    {
        if (non_trivial(scc) &&
            std::find(begin(scc), end(scc), f) != scc.end())
        {
            detail::find_nonempty_path(a, a.initial_state, f, std::back_inserter(result));
            if (result.size() > 0 || a.initial_state == f)
                return true;
        }
        return false;
    });
    if (final_state == a.final_states.end())
        return boost::none;

    std::reverse(result.begin(), result.end());
    std::size_t n = result.size();
    detail::find_nonempty_path(a, *final_state, *final_state, std::back_inserter(result));
    std::reverse(result.begin() + n, result.end());
    return result;
}
}
