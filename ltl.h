#pragma once

#include <memory>
#include <sstream>
#include <vector>
#include <set>

namespace ltl
{
template <typename Prop>
struct formula;

template <typename Prop>
using formula_ptr = std::shared_ptr<formula<Prop>>;

namespace detail
{
template <typename Prop>
std::string to_string_prop(const Prop & p);

template <typename Prop>
std::string to_string_binop(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2, const std::string & op);

template <typename Prop>
std::string to_string_unnop(const formula_ptr<Prop> & f, const std::string & op);
}

template <typename Prop>
struct formula
{
    virtual std::string to_string() const = 0;
    virtual formula_ptr<Prop> negate() const = 0;
    virtual std::vector<formula_ptr<Prop>> subformulas() const = 0;
};

template <typename Prop>
struct neg_proposition_formula;

template <typename Prop>
struct proposition_formula : formula<Prop>
{
    proposition_formula(const Prop & prop)
        : prop(prop)
    {}

    virtual std::string to_string() const override
    {
        return detail::to_string_prop(prop);
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return formula_ptr<Prop>(new neg_proposition_formula<Prop>(prop));
    }

    virtual std::vector<formula_ptr<Prop>> subformulas() const override
    {
        return {};
    }

    Prop prop;
};

template <typename Prop>
struct neg_proposition_formula : formula<Prop>
{
    neg_proposition_formula(const Prop & prop)
        : prop(prop)
    {}

    virtual std::string to_string() const override
    {
        return "not " + detail::to_string_prop(prop);
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return formula_ptr<Prop>(new proposition_formula<Prop>(prop));
    }

    virtual std::vector<formula_ptr<Prop>> subformulas() const override
    {
        return { this->negate() };
    }

    Prop prop;
};

template <typename Prop>
formula_ptr<Prop> prop(const Prop & p)
{
    return formula_ptr<Prop>(new proposition_formula<Prop>(p));
}

template <typename Prop>
formula_ptr<Prop> operator ~ (const formula_ptr<Prop> & f)
{
    return f->negate();
}

template <typename Prop>
struct false_formula;

template <typename Prop>
struct true_formula : formula<Prop>
{
    virtual std::string to_string() const override
    {
        return "true";
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return formula_ptr<Prop>(new false_formula<Prop>);
    }

    virtual std::vector<formula_ptr<Prop>> subformulas() const override
    {
        return {};
    }
};

template <typename Prop>
struct false_formula : formula<Prop>
{
    virtual std::string to_string() const override
    {
        return "false";
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return formula_ptr<Prop>(new true_formula<Prop>);
    }

    virtual std::vector<formula_ptr<Prop>> subformulas() const override
    {
        return {};
    }
};

template <typename Prop>
formula_ptr<Prop> boolean(bool x)
{
    formula_ptr<Prop> result(new true_formula<Prop>);
    if (x)
        return result;
    return ~ result;
}

template <typename Prop>
formula_ptr<Prop> operator && (const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2);

template <typename Prop>
struct binary_formula : formula<Prop>
{
    binary_formula(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
        : f1(f1)
        , f2(f2)
    {}

    virtual std::string op() const = 0;

    virtual std::string to_string() const override
    {
        return detail::to_string_binop(f1, f2, op());
    }

    virtual std::vector<formula_ptr<Prop>> subformulas() const override
    {
        std::vector<formula_ptr<Prop>>
                result1 = f1->subformulas(),
                result2 = f2->subformulas();
        std::copy(result2.begin(), result2.end(), std::back_inserter(result1));
        result1.push_back(f1);
        result1.push_back(f2);
        return result1;
    }

    formula_ptr<Prop> f1, f2;
};

template <typename Prop>
struct or_formula : binary_formula<Prop>
{
    or_formula(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
        : binary_formula<Prop>(f1, f2)
    {}

    virtual std::string op() const override
    {
        return "or";
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return binary_formula<Prop>::f1->negate() &&
               binary_formula<Prop>::f2->negate();
    }
};

template <typename Prop>
formula_ptr<Prop> operator || (const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
{
    return formula_ptr<Prop>(new or_formula<Prop>(f1, f2));
}

template <typename Prop>
struct and_formula : binary_formula<Prop>
{
    and_formula(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
        : binary_formula<Prop>(f1, f2)
    {}

    virtual std::string op() const override
    {
        return "and";
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return binary_formula<Prop>::f1->negate() ||
               binary_formula<Prop>::f2->negate();
    }
};

template <typename Prop>
formula_ptr<Prop> operator && (const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
{
    return formula_ptr<Prop>(new and_formula<Prop>(f1, f2));
}

template <typename Prop>
formula_ptr<Prop> operator >> (const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
{
    return ~ f1 || f2;
}

template <typename Prop>
formula_ptr<Prop> eq(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
{
    return (f1 >> f2) && (f2 >> f1);
}

template <typename Prop>
struct next_formula : formula<Prop>
{
    next_formula(const formula_ptr<Prop> & f)
        : f(f)
    {}

    virtual std::string to_string() const override
    {
        return detail::to_string_unnop(f, "X");
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return formula_ptr<Prop>(new next_formula<Prop>(f->negate()));
    }

    virtual std::vector<formula_ptr<Prop>> subformulas() const override
    {
        std::vector<formula_ptr<Prop>> result = f->subformulas();
        result.push_back(f);
        return result;
    }

    formula_ptr<Prop> f;
};

template <typename Prop>
formula_ptr<Prop> next(const formula_ptr<Prop> & f)
{
    return formula_ptr<Prop>(new next_formula<Prop>(f));
}

template <typename Prop>
struct until_formula : binary_formula<Prop>
{
    until_formula(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
        : binary_formula<Prop>(f1, f2)
    {}

    virtual std::string op() const override
    {
        return "U";
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return release(binary_formula<Prop>::f1->negate(),
                       binary_formula<Prop>::f2->negate());
    }
};

template <typename Prop>
formula_ptr<Prop> until(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
{
    return formula_ptr<Prop>(new until_formula<Prop>(f1, f2));
}

template <typename Prop>
struct release_formula : binary_formula<Prop>
{
    release_formula(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
        : binary_formula<Prop>(f1, f2)
    {}

    virtual std::string op() const override
    {
        return "R";
    }

    virtual formula_ptr<Prop> negate() const override
    {
        return until(binary_formula<Prop>::f1->negate(),
                     binary_formula<Prop>::f2->negate());
    }
};

template <typename Prop>
formula_ptr<Prop> release(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2)
{
    return formula_ptr<Prop>(new release_formula<Prop>(f1, f2));
}

template <typename Prop>
formula_ptr<Prop> future(const formula_ptr<Prop> & f)
{
    return until(boolean<Prop>(true), f);
}

template <typename Prop>
formula_ptr<Prop> globally(const formula_ptr<Prop> & f)
{
    return ~ future(~ f);
}

namespace detail
{
template <typename Prop>
std::string to_string_prop(const Prop & p);

template <>
std::string to_string_prop(const std::string & p)
{
    return p;
}

template <>
std::string to_string_prop(const char & p)
{
    return std::string(1, p);
}

template <typename Prop>
std::string to_string_binop(const formula_ptr<Prop> & f1, const formula_ptr<Prop> & f2, const std::string & op)
{
    std::stringstream ss;
    ss <<  "(" << f1->to_string() << ") "
       << op
       << " (" << f2->to_string() << ")";
    return ss.str();
}

template <typename Prop>
std::string to_string_unnop(const formula_ptr<Prop> & f, const std::string & op)
{
    std::stringstream ss;
    ss << op << " (" << f->to_string() << ")";
    return ss.str();
}
}

template <typename Prop>
std::size_t hash_value(const ltl::formula_ptr<Prop> & f)
{
    return std::hash<std::string>()(f->to_string());
}

template <typename Prop>
bool operator == (const ltl::formula_ptr<Prop> & a,
                  const ltl::formula_ptr<Prop> & b)
{
    return a->to_string() == b->to_string();
}

template <typename Prop>
bool operator != (const ltl::formula_ptr<Prop> & a,
                  const ltl::formula_ptr<Prop> & b)
{
    return !(a == b);
}

template <typename Prop>
struct formula_comparator
{
    bool operator ()(const ltl::formula_ptr<Prop> & a,
                     const ltl::formula_ptr<Prop> & b) const
    {
        return a->to_string() < b->to_string();
    }
};

template <typename FormulaType, typename InputIterator>
std::set<decltype(std::declval<FormulaType>().prop)> props(InputIterator begin, InputIterator end)
{
    using Prop = decltype(std::declval<FormulaType>().prop);
    std::set<Prop> result;
    for (; begin != end; ++begin)
    {
        auto p = dynamic_cast<FormulaType *>(begin->get());
        if (p)
            result.insert(p->prop);
    }
    return result;
}

template <typename Prop, typename InputIterator>
std::set<Prop> true_props(InputIterator begin, InputIterator end)
{
    return props<ltl::proposition_formula<Prop>>(begin, end);
}

template <typename Prop, typename InputIterator>
std::set<Prop> false_props(InputIterator begin, InputIterator end)
{
    return props<ltl::neg_proposition_formula<Prop>>(begin, end);
}

template <typename Prop>
std::set<Prop> all_props(const ltl::formula_ptr<Prop> & f)
{
    std::set<Prop> result;
    auto const & sf = f->subformulas();
    auto t_props = true_props<Prop>(sf.begin(), sf.end());
    auto f_props = false_props<Prop>(sf.begin(), sf.end());
    std::copy(t_props.begin(), t_props.end(), std::inserter(result, result.begin()));
    std::copy(f_props.begin(), f_props.end(), std::inserter(result, result.begin()));
    return result;
}

template <typename Prop>
bool containsTemporalOperators(const ltl::formula_ptr<Prop>& f)
{
    // DFS the formula, if get to the leaf without meeting temporal operator -> can't prove
    // check type, return true
    formula<Prop>* ptr = f.get();
    if(   dynamic_cast<next_formula<Prop>*>(ptr)
       || dynamic_cast<until_formula<Prop>*>(ptr)
       || dynamic_cast<release_formula<Prop>*>(ptr)) return true;
    
    for(const auto &child : f->subformulas()) if(containsTemporalOperators(child)) return true;
    return false;
}
}
