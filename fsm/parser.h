#pragma once

#include "fsm.h"

namespace fsm
{
automaton_t load_model(std::istream &input);
}
