#include "parser.h"

#include <tinyxml2.h>

#include <algorithm>
#include <istream>
#include <unordered_map>
#include <utility>

namespace fsm
{
automaton_t load_model(std::istream &input)
{
    std::istreambuf_iterator<char> eos;
    std::string str(std::istreambuf_iterator<char>(input), eos);
    tinyxml2::XMLDocument in;
    in.Parse(str.c_str());

    const auto root = in.FirstChildElement("diagram");

    std::vector<state> states;
    std::unordered_map<std::size_t, events_t> events;
    automaton_t::labels_t all_labels;
    for(auto el = root->FirstChildElement("widget"); el != nullptr; el = el->NextSiblingElement("widget"))
    {
        int id = el->IntAttribute("id", 0);
        std::string type = el->Attribute("type");
        const auto attributes = el->FirstChildElement("attributes");
        if (type == "Transition")
        {
            for(auto ch = attributes->FirstChildElement("event"); ch != nullptr; ch = ch->NextSiblingElement("event"))
            {
                events[id].insert(ch->Attribute("name"));
                all_labels.insert(ch->Attribute("name"));
            }
            continue;
        }
        if(type != "State") continue;

        state s;
        s.name = attributes->FirstChildElement("name")->GetText();
        s.type = attributes->FirstChildElement("type")->IntText(0);
        for(auto ch = attributes->FirstChildElement("outgoing"); ch != nullptr; ch = ch->NextSiblingElement("outgoing"))
        {
            s.outgoing.insert(ch->IntAttribute("id", 0));
        }
        for(auto ch = attributes->FirstChildElement("incoming"); ch != nullptr; ch = ch->NextSiblingElement("incoming"))
        {
            s.incoming.insert(ch->IntAttribute("id", 0));
        }
        states.push_back(s);
    }

    automaton_t::transitions_t transitions;

    for(size_t i = 0; i < states.size(); i++)
    {
        const auto &state = states[i];

        for(size_t j = 0; j < states.size(); j++)
        {
            const auto &other = states[j];
            for(const auto &from : state.outgoing)
            {
                if(other.incoming.find(from) != other.incoming.end())
                    transitions[i].insert({events[from], j});
            }
        }
    }

    return automaton_t{states, transitions, all_labels};
}
}
