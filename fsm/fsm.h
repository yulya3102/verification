#pragma once

#include <boost/functional/hash.hpp>

#include <unordered_set>
#include <unordered_map>
#include <set>

namespace fsm
{
struct state
{
    std::string name;
    int type;
    std::unordered_set<int> incoming;
    std::unordered_set<int> outgoing;
};

using events_t = std::set<std::string>;

struct automaton_t
{
    using state_idx = std::size_t;
    using states_t =  std::vector<state>;
    using outgoing_transitions_t = std::unordered_multimap<events_t, state_idx, boost::hash<events_t>>;
    using transitions_t = std::unordered_map<state_idx, outgoing_transitions_t>;
    using labels_t = std::set<std::string>;

    states_t states;
    transitions_t transition;
    labels_t all_labels;
};
}
