#pragma once

#include "ltl.h"
#include "buchi_ltl.h"

#include <buchi/example.h>
#include <buchi/intersection.h>

#include <boost/optional.hpp>

template <typename Prop>
boost::optional<typename buchi::automaton<Prop>::Word>
    find_counterexample(const buchi::automaton<Prop> & model,
                        const ltl::formula_ptr<Prop> & f)
{
    auto buchi_ltl = buchi::from_ltl(model.all_props, ~ f);
    auto buchi_counter = model && buchi_ltl;
#ifdef DEBUG
    std::ofstream ltl_out("ltl.dot");
    ltl_out << buchi_ltl << std::endl;
    std::ofstream fsm_out("fsm.dot");
    fsm_out << model << std::endl;
    std::ofstream counter_out("counter.dot");
    counter_out << buchi_counter << std::endl;
#endif
    return buchi::find_example(buchi_counter);
}
