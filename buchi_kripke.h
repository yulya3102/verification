#pragma once

#include "kripke.h"

#include <buchi/buchi.h>

#include <unordered_set>

namespace buchi
{
template <typename Prop>
automaton<Prop> from_kripke(const kripke_model<Prop> & model)
{
    using kripke = kripke_model<Prop>;
    using res_t = automaton<Prop>;
    automaton<Prop> result;

    std::unordered_set<Prop> all_labels;
    for(const auto &labels : model.labels)
    {
        all_labels.insert(labels.second.begin(), labels.second.end());
    }

    const typename res_t::state_t new_initial = model.states_count;
    result.initial_state = new_initial;
    result.states_count = model.states_count + 1;

    for(typename kripke::state_t state = 0; state < result.states_count; state++) result.final_states.push_back(state);

    for(const auto &old_initial : model.initial_states)
    {
        typename res_t::TrueProps true_set;
        typename res_t::FalseProps false_set;
        const typename kripke::Labels &labels = model.labels.find(old_initial)->second;
        true_set.insert(labels.begin(), labels.end());
        false_set.insert(all_labels.begin(), all_labels.end());
        for(const auto &true_element : true_set) false_set.erase(true_element);
        result.transition[new_initial].insert(make_pair(typename res_t::TransitionLabel(true_set, false_set), old_initial));
    }

    for(const auto &old_transition : model.transition)
    {
        for(const auto &old_transition_target : old_transition.second)
        {
            typename res_t::TrueProps true_set;
            typename res_t::FalseProps false_set;
            const typename kripke::Labels &labels = model.labels.find(old_transition_target)->second;
            true_set.insert(labels.begin(), labels.end());
            false_set.insert(all_labels.begin(), all_labels.end());
            for(const auto &true_element : true_set) false_set.erase(true_element);
            result.transition[old_transition.first].insert(make_pair(typename res_t::TransitionLabel(true_set, false_set),
                                                                     old_transition_target));
        }
    }


    return result;
}
}
